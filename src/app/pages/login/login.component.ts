import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { Usuario } from './usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private usuario: Usuario = new Usuario();

  constructor(private authService: AuthService) {}

  ngOnInit() {
  }
  
  ngOnDestroy() {
  }

  fazerLogin(){
    console.log(this.usuario);
  }

}
