import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";

@Component({
  selector: 'app-page-teste',
  templateUrl: './page-teste.component.html',
  styleUrls: ['./page-teste.component.scss']
})
export class PageTeste implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;

  public valorAtual: String = "";
  public valorSalvo: String = "";
  public valorFocus: String = "";

  public variavelCompartilhada: String = "Compartilhada ok!";


  constructor() { }

  ngOnInit() {

  }

  botaoClicado() {
    alert('voce clicou aqui!');
  }

  inputComEvento(evento: KeyboardEvent) {
    this.valorAtual =(<HTMLInputElement>evento.target).value;
  }

  escutarEnter(valor){
    this.valorSalvo = valor;
  }

  perdeFocus(valor){
    this.valorFocus = valor;
  }



}
