import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTeste } from './page-teste.component';

describe('PageTeste', () => {
  let component: PageTeste;
  let fixture: ComponentFixture<PageTeste>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTeste ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTeste);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
