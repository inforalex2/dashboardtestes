import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-two-way',
  templateUrl: './two-way.component.html',
  styleUrls: ['./two-way.component.scss']
})
export class TwoWay implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;

  public valorDigitado: String = "";
  
  /** recebendo objeto ou variavel compartilhada  */
  @Input() nome:String = "";
  
  /**objeto pessoa */
  pessoa: any = {
    nome: 'abc',
    idade: 20
  }

  public valorInput:number = 0;

  botaoSobe(){
    this.valorInput++;
  }

  botaoDece(){
    this.valorInput--;
  }

  constructor() { }

  ngOnInit() {

  }

  

}
