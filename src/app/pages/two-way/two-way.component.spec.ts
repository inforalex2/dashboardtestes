import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoWay } from './two-way.component';

describe('TwoWay', () => {
  let component: TwoWay;
  let fixture: ComponentFixture<TwoWay>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoWay ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoWay);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
