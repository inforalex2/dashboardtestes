import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Teste123Component } from './teste123.component';

describe('Teste123Component', () => {
  let component: Teste123Component;
  let fixture: ComponentFixture<Teste123Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Teste123Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Teste123Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
